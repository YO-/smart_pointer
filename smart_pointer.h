#include <iostream>
#include <assert.h>

using namespace std;
template <typename T>
struct smart_ptr{
    smart_ptr(T *arg = nullptr): p(arg){
        if (p){
            ref_counts=new unsigned int(1);
        } else{
            ref_counts=new unsigned int(0);
        }
        cout<<*ref_counts<<endl;
    }
    T &operator*(){
        assert(this->p != nullptr);
        return *(this->p);
    }

    smart_ptr &operator=(const smart_ptr &pointer){
        if(&pointer == this)
            return *this;
        (*this->ref_counts)--;
        if((*this->ref_counts) == 0){
            delete this->p;
            delete this->ref_counts;
        }
        this->ref_counts = pointer.ref_counts;
        this->p = pointer.p;
        (*this->ref_counts)++;

        cout<<*ref_counts<<endl;
        return *this;
    }

    smart_ptr(const smart_ptr &pointer){

        if(&pointer!=this) {
            this->ref_counts = pointer.ref_counts;
            (*this->ref_counts)++;
            this->p = pointer.p;
            cout << *ref_counts << endl;
        } else{
            this->p = nullptr;
            this->ref_counts = new unsigned int(0);
        }

    }

    ~smart_ptr(){
        if(*ref_counts!=0){
            (*ref_counts)--;
            cout<<*ref_counts<<endl;
            if(*ref_counts==0){
                delete p;
                delete ref_counts;
            }
        }
    }
private:
    T* p;
    unsigned int* ref_counts;
};



int main() {
   smart_ptr<int> ab,bb(new int(5555));
   ab = bb;
   bb = ab;
}